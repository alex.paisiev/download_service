import traceback
from datetime import datetime

from django.http import FileResponse
from django.shortcuts import render
from django.conf import settings

from rest_framework.views import APIView
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import filters
from rest_framework.exceptions import APIException

from download_service.logging import add_logging, current_time_UTC
from file_service.models import ViaFile
from file_service.serializers import ViaFileSerializer


@api_view(['GET', ])
def download_via_file(request):
    if request.method == 'GET':
        file_identifiers = request.GET.get('file_identifiers', False)
    if file_identifiers:
        try:
            requested_files = ViaFile.objects.get(title=file_identifiers)
            response = FileResponse(open(requested_files.file.path, 'rb'))
            response['Content-Disposition'] = f'attachment; filename={requested_files.file.name}'
            return response
        except Exception as e:
            print(e)
            full_traceback = str(traceback.format_exc())
            add_logging({'timestamp': current_time_UTC(), 'level': 'error', 'traceback': full_traceback})
            raise APIException({'file': 'no file found'})


@api_view(['GET', ])
def search_file(request):
    app_name = request.query_params.get('appName')
    app_os = request.query_params.get('os')
    app_version = request.query_params.get('version')
    results_limit = request.query_params.get('limit')

    search_files = ViaFile.objects.all()

    if app_name:
        search_files = search_files.filter(app__title=app_name).order_by('app__version')
    if app_version:
        search_files = search_files.filter(app__version=app_version).order_by('app__os')
    if app_os:
        search_files = search_files.filter(app__os=app_os)
    else:
        search_files = search_files.order_by('app__title')

    if results_limit:
        try:
            results_limit = int(results_limit)
        except Exception as e:
            print(e)
            full_traceback = str(traceback.format_exc())
            add_logging({'timestamp': current_time_UTC(), 'level': 'error', 'traceback': full_traceback})

            raise APIException({'limit': 'limit should be a an integer'})

        search_files = search_files[:results_limit]
    serializer = ViaFileSerializer(search_files, many=True)

    return Response(serializer.data)
