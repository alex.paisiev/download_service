from django.db import models
from django.core.files import File
from file_service.choices import OS_TYPES


class ViaApp(models.Model):
    title = models.CharField(max_length=255)
    version = models.IntegerField()
    os = models.CharField(max_length=10, choices=OS_TYPES)

    def __str__(self):
        return f'{self.title} - {self.os} - v.{self.version}'


class ViaFile(models.Model):
    title = models.CharField(max_length=255)
    file = models.FileField(upload_to='uploads')
    app = models.ForeignKey(ViaApp, related_name='via_apps', null=True, on_delete=models.SET_NULL)

    def __str__(self):
        return f'{self.title}: {self.app}'
