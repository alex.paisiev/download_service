from rest_framework import serializers

from file_service.models import ViaFile, ViaApp


class ViaAppSerializer(serializers.ModelSerializer):
    class Meta:
        model = ViaApp
        fields = ('title', 'os', 'version')


class ViaFileSerializer(serializers.ModelSerializer):
    app_name = serializers.CharField(source='app.title', read_only=True)
    app_version = serializers.IntegerField(source='app.version', read_only=True)
    app_os = serializers.CharField(source='app.get_os_display', read_only=True)
    file_name = serializers.CharField(source='file,name', read_only=True)

    class Meta:
        model = ViaFile
        fields = ('id', 'title', 'file_name', 'app_name', 'app_version', 'app_os')
