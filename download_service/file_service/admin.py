from django.contrib import admin

from file_service.models import ViaApp, ViaFile


@admin.register(ViaApp)
class ViaAppAdmin(admin.ModelAdmin):
    pass


@admin.register(ViaFile)
class ViaFileAdmin(admin.ModelAdmin):
    pass
