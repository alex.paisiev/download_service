import json
import traceback
from datetime import datetime


def add_logging(log_dict: dict):
    loggings_file = 'download_service/log.json'

    with open(loggings_file) as f:
        data = json.load(f)
    data[current_time_UTC()] = log_dict

    with open(loggings_file, 'w') as f:
        json.dump(data, f, indent=4, separators=(',', ':'))


def current_time_UTC():
    return datetime.now().strftime('%d/%m/%Y %H:%M:%S')
