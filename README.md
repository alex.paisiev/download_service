# download_service

Download service created with Django REST framework

## Project setup:

1. Create virtual environment 

2. Navigate to project folder and run "pip install -r requirements.txt"

3. Create a database for the project and create a "dev.py" from the "dev_sample.py" in the project settings directory

4. Run "python manage.py migrate" 

5. Navigate to hostname/admin to create a couple of ViaApp objects and ViaFile objects
